from db.sqlite import actions

LISTINGS = """
    CREATE TABLE listings (
        pk integer primary key autoincrement,  -- listing id
        uin text,  -- dao item id
        url text,  -- dao item url
        user_id integer,  -- telegram user
        user_name text,
        price integer,  -- item price
        comment text,
        is_active integer,  -- temporarily delist
        ts_created integer,
        ts_updated integer
    )
"""


def initialize():
    conn = actions.connect()
    conn.execute(LISTINGS)


if __name__ == "__main__":
    initialize()
