import sqlite3

import settings
from functions import timestamp_now
from models import Listing


def connect():
    settings.DATABASE_PATH.parent.mkdir(exist_ok=True)
    conn = sqlite3.connect(settings.DATABASE_PATH)
    return conn


def listing_create(listing: Listing):
    conn = connect()
    now = int(timestamp_now())
    r = conn.execute(
        "INSERT INTO"
        " listings (uin, url, user_id, user_name, price, comment, is_active, ts_created, ts_updated)"
        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
        listing.insert() + (now, now),
    )
    conn.commit()
    return r


def listing_list():
    conn = connect()
    r = conn.execute(
        "SELECT pk, uin, url, user_id, user_name, price, comment, is_active, ts_created, ts_updated FROM listings"
        " ORDER BY ts_created, ts_updated, uin, pk;"
    ).fetchall()
    return [Listing(*row) for row in r]


def my_listing_list(user_id: int):
    conn = connect()
    r = conn.execute(
        "SELECT pk, uin, url, user_id, user_name, price, comment, is_active, ts_created, ts_updated FROM listings"
        " WHERE user_id = ?"
        " ORDER BY ts_created, ts_updated, uin, pk;",
        (user_id,)
    ).fetchall()
    return [Listing(*row) for row in r]


def listing_search(uins: list[str]):
    conn = connect()
    r = conn.execute(
        "SELECT pk, uin, url, user_id, user_name, price, comment, is_active, ts_created, ts_updated FROM listings"
        f" WHERE uin in ({','.join(['?'] * len(uins))})"
        " ORDER BY price DESC, ts_created, ts_updated, uin, pk;",
        uins,
    ).fetchall()
    return [Listing(*row) for row in r]


def delist(user_id: int, uin: str):
    conn = connect()
    count = conn.execute(
        "SELECT COUNT(*) FROM listings WHERE user_id = ? AND uin = ?;",
        (user_id, uin),
    ).fetchall()[0][0]
    if count == 0:
        return 0
    conn.execute(
        "DELETE FROM listings WHERE user_id = ? AND uin = ?;",
        (user_id, uin),
    )
    conn.commit()
    return count
