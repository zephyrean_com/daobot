import string
from dataclasses import dataclass
from typing import Optional

import requests
from bs4 import BeautifulSoup


class ValidationError(Exception):
    pass


@dataclass
class Product:
    uin: str


def validate_uin(uin: str):
    if not uin or any(ch not in string.digits for ch in uin):
        raise ValidationError


def _get_uin(soup: BeautifulSoup):
    div = soup.find("div", class_="ut2-pb__sku")
    if div is None:
        return None
    span = div.find("span", class_="ty-control-group__item")
    if span is None:
        return None

    uin = span.text
    validate_uin(uin)

    return uin


def parse_product_page(url: str) -> Optional[Product]:
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        return None

    if r.status_code != 200:
        return None

    soup = BeautifulSoup(r.text, "lxml")
    try:
        uin = _get_uin(soup)
    except ValidationError:
        return None

    return Product(uin=uin)
