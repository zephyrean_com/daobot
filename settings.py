import os
from pathlib import Path

TOKEN = os.getenv("TELEGRAM_TOKEN", "")

BASE_DIR = Path(__file__).resolve().parent

DATA_PATH = BASE_DIR / "_data"
DATABASE_NAME = "db.sqlite3"
DATABASE_PATH = DATA_PATH / DATABASE_NAME
