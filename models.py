import datetime
from dataclasses import dataclass
from typing import Optional


@dataclass
class Listing:
    pk: Optional[int]
    uin: Optional[str]
    url: Optional[str]
    user_id: int
    user_name: str
    price: Optional[int]
    comment: Optional[str]
    is_active: bool
    ts_created: Optional[int] = None
    ts_updated: Optional[int] = None

    @staticmethod
    def q_if_none(v):
        return v if v is not None else '?'

    def uncommitted(self):
        return "\n".join((
            f"Объявление размещается от аккаунта '{self.user_name}'.",
            f"Артикул товара: {self.q_if_none(self.uin)}",
            f"Цена товара: {self.q_if_none(self.price)} руб.",
            f"Комментарий: {self.q_if_none(self.comment)}",
        ))

    def list(self):
        return "\n".join((
            # f"Номер п/п: {self.pk}",
            f"Размещено: {datetime.datetime.fromtimestamp(self.ts_created)}",
            f"Продавец: {self.user_name}",
            f"Артикул: {self.uin}",
            f"Цена товара: {self.price} руб.",
            f"Комментарий: {self.comment}",
            f"Ссылка: {self.url}",
        ))

    def my_list(self):
        return "\n".join((
            f"Размещено: {datetime.datetime.fromtimestamp(self.ts_created)}",
            f"Артикул: {self.uin}",
            f"Цена товара: {self.price} руб.",
            f"Комментарий: {self.comment}",
            f"Ссылка: {self.url}",
        ))

    def insert(self):
        return (
            self.uin, self.url, self.user_id, self.user_name, self.price, self.comment, self.is_active
        )
