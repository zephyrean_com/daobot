import urllib.parse

from telegram import (
    ReplyKeyboardMarkup,
    Update,
)
from telegram.ext import ContextTypes

import store_interface.products
from db.sqlite import actions
from functions import log
from models import Listing
from . import (
    main_menu,
    states,
)


@log
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    listing = Listing(
        pk=None,
        uin=None,
        url=None,
        user_id=update.effective_user.id,
        user_name=update.effective_user.name,
        price=None,
        comment=None,
        is_active=True,
    )
    context.user_data["listing"] = listing

    prompt = "Введите ссылку на товар на сайте daochai.ru:"
    await update.message.reply_text(
        "\n\n".join((listing.uncommitted(), prompt)),
        reply_markup=ReplyKeyboardMarkup([["Отмена"]])
    )
    return states.NEW_LISTING_URL


@log
async def url(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    listing = context.user_data["listing"]

    link = update.message.text.strip()
    parsed = urllib.parse.urlparse(link)
    error_msg = ""
    if parsed.hostname != "daochai.ru":
        error_msg = "Ссылка не на daochai.ru!"
    else:
        product = store_interface.products.parse_product_page(link)
        if product is not None:
            listing.uin = product.uin
            listing.url = link
        else:
            error_msg = "Не получается определить артикул товара."
    if error_msg:
        await update.message.reply_text(
            error_msg,
            reply_markup=ReplyKeyboardMarkup([["Отмена"]]),
        )
        return states.NEW_LISTING_URL

    prompt = "Введите цену в рублях (максимум 1 млн руб):"
    await update.message.reply_text(
        "\n\n".join((listing.uncommitted(), prompt)),
        reply_markup=ReplyKeyboardMarkup([["Отмена"]]),
    )
    return states.NEW_LISTING_PRICE


@log
async def price(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    listing = context.user_data["listing"]

    price_str = update.message.text.strip()
    error_msg = ""
    try:
        listing.price = int(price_str)
    except ValueError:
        error_msg = "Введите целое число от 0 до 1'000'000"
    else:
        if not 0 <= listing.price <= 1_000_000:
            error_msg = "Введите целое число от 0 до 1'000'000"

    if error_msg:
        await update.message.reply_text(
            error_msg,
            reply_markup=ReplyKeyboardMarkup([["Отмена"]])
        )
        return states.NEW_LISTING_PRICE

    prompt = f"Оставьте комментарий:"
    await update.message.reply_text(
        "\n\n".join((listing.uncommitted(), prompt)),
        reply_markup=ReplyKeyboardMarkup([["Не буду"], ["Отмена"]])
    )
    return states.NEW_LISTING_COMMENT


@log
async def comment(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    listing = context.user_data["listing"]

    comment_text = update.message.text.strip()
    if comment_text == "Не буду":
        comment_text = ""
    listing.comment = comment_text

    prompt = f"Опубликовать объявление?"
    await update.message.reply_text(
        "\n\n".join((listing.uncommitted(), prompt)),
        reply_markup=ReplyKeyboardMarkup([["Опубликовать"], ["Отмена"]])
    )
    return states.NEW_LISTING_PUBLISH


@log
async def publish(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    try:
        actions.listing_create(context.user_data["listing"])
    except Exception:
        await update.message.reply_text("Ошибка при сохранении объявления!", reply_markup=main_menu.main_markup)
        raise
    else:
        await update.message.reply_text("Объявление размещено успешно!", reply_markup=main_menu.main_markup)
    finally:
        del context.user_data["listing"]
        return states.MAIN_MENU


@log
async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Отмена!", reply_markup=main_menu.main_markup)
    try:
        del context.user_data["listing"]
    except KeyError:
        pass
    return states.MAIN_MENU
