from telegram import (
    ReplyKeyboardMarkup,
    Update,
)
from telegram.ext import ContextTypes

import store_interface.products
from db.sqlite import actions
from functions import log
from . import (
    main_menu,
    states,
)


@log
async def search(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    prompt = "Введите артикулы товаров (числа) через пробел:"
    await update.message.reply_text(prompt, reply_markup=ReplyKeyboardMarkup([["Назад"]]))
    return states.SEARCH_LISTINGS


@log
async def perform_search(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    uins = [v for v in update.message.text.split()]
    try:
        for uin in uins:
            store_interface.products.validate_uin(uin)
    except store_interface.products.ValidationError:
        error_msg = "Введите артикулы товаров (числа) через пробел."
        await update.message.reply_text(error_msg, reply_markup=ReplyKeyboardMarkup([["Назад"]]))
        return states.SEARCH_LISTINGS

    if not uins:
        error_msg = "Введите артикулы товаров (числа) через пробел."
        await update.message.reply_text(error_msg, reply_markup=ReplyKeyboardMarkup([["Назад"]]))
        return states.SEARCH_LISTINGS

    listings = actions.listing_search(uins=uins)

    message = "\n\n".join(e.list() for e in listings) + "\n\n" + f"Всего найдено: {len(listings)}"

    await update.message.reply_text(message, reply_markup=ReplyKeyboardMarkup([["Назад"]]))
    return states.SEARCH_LISTINGS


@log
async def back(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Назад!", reply_markup=main_menu.main_markup)
    try:
        del context.user_data["listing"]
    except KeyError:
        pass
    return states.MAIN_MENU
