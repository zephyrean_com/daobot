from telegram import (
    ReplyKeyboardMarkup,
    Update,
)
from telegram.ext import ContextTypes

from db.sqlite import actions
from functions import log
from . import states

main_reply_keyboard = [
    ["Разместить", "Мои объявления", "Все объявления", "Найти", "Отзыв"],
]
main_markup = ReplyKeyboardMarkup(main_reply_keyboard, one_time_keyboard=True)


@log
async def view_listings(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    listings = actions.listing_list()

    message = "\n\n".join(e.list() for e in listings) + "\n\n" + f"Всего: {len(listings)}"

    await update.message.reply_text(message, reply_markup=main_markup)
    return states.MAIN_MENU


@log
async def feedback(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Напишите аккаунту @zefirina", reply_markup=main_markup)
    return states.MAIN_MENU
