from telegram import (
    ReplyKeyboardMarkup,
    Update,
)
from telegram.ext import ContextTypes

from db.sqlite import actions
from functions import log
from store_interface.products import (
    ValidationError,
    validate_uin,
)
from . import (
    main_menu,
    states,
)


@log
async def my_listings(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    listings = actions.my_listing_list(user_id=update.effective_user.id)

    message = "\n\n".join(e.my_list() for e in listings) + "\n\n" + f"Всего: {len(listings)}"

    await update.message.reply_text(
        message,
        reply_markup=ReplyKeyboardMarkup([["Снять объявление"], ["Назад"]], one_time_keyboard=True),
    )
    return states.MY_LISTINGS


@log
async def delist(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    prompt = "Введите артикул товара (число):"
    await update.message.reply_text(
        prompt,
        reply_markup=ReplyKeyboardMarkup([["Назад"]], one_time_keyboard=True),
    )
    return states.DELIST


@log
async def perform_delist(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    uin = update.message.text.strip()
    try:
        validate_uin(uin)
    except ValidationError:
        error_msg = "Введите артикул товара (число):"
        await update.message.reply_text(
            error_msg,
            reply_markup=ReplyKeyboardMarkup([["Назад"]], one_time_keyboard=True),
        )
        return states.DELIST

    delisted_count = actions.delist(update.effective_user.id, uin)
    if delisted_count > 0:
        message = f"Артикул {uin} снят."
    else:
        message = f"Артикул {uin} не найден."
    await update.message.reply_text(message, reply_markup=main_menu.main_markup)
    return states.MAIN_MENU


@log
async def back(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Назад!", reply_markup=main_menu.main_markup)
    try:
        del context.user_data["listing"]
    except KeyError:
        pass
    return states.MAIN_MENU
