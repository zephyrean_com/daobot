from telegram import (
    ReplyKeyboardRemove,
    Update,
)
from telegram.ext import (
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

from . import (
    main_menu,
    my_listings,
    new_listing,
    search_listings,
    states,
)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Start the conversation and ask user for input."""
    try:
        del context.user_data["listing"]
    except KeyError:
        pass
    await update.message.reply_text(
        "Здравствуйте! Это бот по размещению объявлений о продаже посуды из ассортимента DAOchai.ru."
        "\n\n"
        # "Чтобы пользоваться ботом, нужно состоять в группе https://t.me/daochai_chat"
        # "\n\n"
        "Для выхода напишите \"/done\"."
        "\n"
        "Для повторного входа напишите \"/start\".",
        reply_markup=main_menu.main_markup,
    )

    return states.MAIN_MENU


async def done(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Display the gathered info and end the conversation."""
    try:
        del context.user_data["listing"]
    except KeyError:
        pass

    await update.message.reply_text(
        f"Выход!",
        reply_markup=ReplyKeyboardRemove(),
    )

    context.user_data.clear()
    return ConversationHandler.END


def make_handler():
    # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            states.MAIN_MENU: [
                MessageHandler(filters.Text(["Разместить"]) & ~filters.COMMAND, new_listing.start),
                MessageHandler(filters.Text(["Мои объявления"]) & ~filters.COMMAND, my_listings.my_listings),
                MessageHandler(filters.Text(["Все объявления"]) & ~filters.COMMAND, main_menu.view_listings),
                MessageHandler(filters.Text(["Найти"]) & ~filters.COMMAND, search_listings.search),
                MessageHandler(filters.Text(["Отзыв"]) & ~filters.COMMAND, main_menu.feedback),
            ],
            states.NEW_LISTING_URL: [
                MessageHandler(filters.Text(["Отмена"]) & ~filters.COMMAND, new_listing.cancel),
                MessageHandler(filters.TEXT & ~filters.COMMAND, new_listing.url)
            ],
            states.NEW_LISTING_PRICE: [
                MessageHandler(filters.Text(["Отмена"]) & ~filters.COMMAND, new_listing.cancel),
                MessageHandler(filters.TEXT & ~filters.COMMAND, new_listing.price),
            ],
            states.NEW_LISTING_COMMENT: [
                MessageHandler(filters.Text(["Отмена"]) & ~filters.COMMAND, new_listing.cancel),
                MessageHandler(filters.TEXT & ~filters.COMMAND, new_listing.comment),
            ],
            states.NEW_LISTING_PUBLISH: [
                MessageHandler(filters.Text(["Отмена"]) & ~filters.COMMAND, new_listing.cancel),
                MessageHandler(filters.Text(["Опубликовать"]) & ~filters.COMMAND, new_listing.publish)
            ],
            states.MY_LISTINGS: [
                MessageHandler(filters.Text(["Назад"]) & ~filters.COMMAND, my_listings.back),
                MessageHandler(filters.TEXT & ~filters.COMMAND, my_listings.delist)
            ],
            states.DELIST: [
                MessageHandler(filters.Text(["Назад"]) & ~filters.COMMAND, my_listings.back),
                MessageHandler(filters.TEXT & ~filters.COMMAND, my_listings.perform_delist)
            ],
            states.SEARCH_LISTINGS: [
                MessageHandler(filters.Text(["Назад"]) & ~filters.COMMAND, search_listings.back),
                MessageHandler(filters.TEXT & ~filters.COMMAND, search_listings.perform_search),
            ]
        },
        fallbacks=[CommandHandler("done", done)],
    )
    return conv_handler
