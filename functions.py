import functools
import logging
from datetime import datetime

logger = logging.getLogger("telegram")


def log(func):
    def proxy(*a, **kw):
        try:
            return func(*a, **kw)
        except Exception:
            logger.exception("..")
            raise

    return functools.wraps(func)(proxy)


def timestamp_now():
    return datetime.utcnow().timestamp()
